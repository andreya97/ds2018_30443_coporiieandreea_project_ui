import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Article} from '../model/article';
import {ArticleService} from '../services/article.service';
import {CategoryService} from '../services/category.service';
import {Category} from '../model/category';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {

  articles: Article[] = [];
  categories: Category[] = [];
  displayedColumns: string[] = ['title', 'author', 'category', 'view', 'delete'];
  currentUser: string = localStorage.getItem('username');
  errorMsg = false;
  chosenCateg: Category;

  constructor(private router: Router, private articleService: ArticleService, private categoryService: CategoryService) {
  }

  ngOnInit() {
    this.articleService.getArticles()
      .subscribe(data => {
        this.articles = data;

        console.log(this.articles);
        this.articles.forEach((art) => art.categoryId !== null ?
          this.categoryService.getCategoryById(art.categoryId).subscribe((a) => art.category = a) : null);
      });
    this.categoryService.getCategories().subscribe(data => {
      this.categories = data;
    });
  }

  viewDetails(id: number): void {
    this.router.navigate(['article', id]);
  }

  deleteArt(id: number): void {
    this.articleService.deleteArticle(id).subscribe(a => window.location.reload());
  }

  onChange(ev): void {
    this.errorMsg = false;
    this.chosenCateg = ev.value;
    const articleIds: number[] = this.chosenCateg.articleIds;
    const data: Article[] = [];
    articleIds.forEach((id) => this.articleService.getArticleById(id).subscribe((art => {
      art.category = this.chosenCateg;
      data.push(art);
    })));
    this.articles = data;
    if (articleIds.length === 0) {
      this.errorMsg = true;
    }
  }
}
