import {Category} from './category';

export class Article {
  id: number;
  title: string;
  author: string;
  body: string;
  categoryId: number;
  category: Category = new Category();
}
