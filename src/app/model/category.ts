export class Category {
  id: number;
  name: string;
  description: string;
  articleIds: number[];
  subscriberIds: number[];
}
