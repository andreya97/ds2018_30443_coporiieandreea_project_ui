export class User {
  id: number;
  username: string;
  password: string;
  email: string;
  admin: boolean;
  subscriptionIds: number[];
}
