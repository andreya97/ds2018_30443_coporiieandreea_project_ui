import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {User} from '../model/user';
import {RootService} from '../services/root.service';
import {LoginInfo} from '../model/logininfo';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  message: string;
  username: string;
  email: string;
  password: string;
  user: User = new User();
  loginfo = new LoginInfo();
  constructor(private router: Router, private rootService: RootService) {
  }

  ngOnInit(): void {
    localStorage.clear();
  }

  doLogin() {
    if (this.username === null || this.username === undefined || this.password === null || this.password === undefined) {
      this.message = 'Please fill in username and password.';
    }
    this.loginfo.username = this.username; this.loginfo.password = this.password;
    this.rootService.login(this.loginfo).subscribe(resp => {
      if (resp === null || resp === undefined) {
        this.message = 'Invalid login information.';
      } else {
        localStorage.setItem('username', resp.username);
        localStorage.setItem('userid', resp.id.toString());
        if (resp.admin === true) {localStorage.setItem('role', 'ADMIN'); }
        if (resp.admin === false) {localStorage.setItem('role', 'USER'); }
        this.router.navigate(['home']);
      }
    }, error => this.message = error.error);
  }
  doRegister() {
    if (this.username === null || this.username === undefined || this.password === null || this.password === undefined ||
                this.email === null || this.email === undefined) {
      this.message = 'Please fill in all fields for registration.';
    } else {
      this.user.username = this.username; this.user.password = this.password; this.user.email = this.email;
      this.rootService.register(this.user).subscribe(resp => {
        this.message = 'You have successfully registered. Please log in.';
      }, error => this.message = error.error);
    }
  }
  doRedirect() {
    localStorage.clear();
    localStorage.setItem('role', 'NONE');
    this.router.navigate(['home']);
  }
}
