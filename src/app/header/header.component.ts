import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router, RoutesRecognized} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isAdmin = false;
  isUser = false;
  isLoginPage;

  constructor(private router: Router) {
    router.events.subscribe((val) => {
     if (val instanceof NavigationEnd) {
       this.isAdmin = localStorage.getItem('role') === 'ADMIN';
       this.isUser = localStorage.getItem('role') === 'USER';
       this.isLoginPage = window.location.pathname === '/' || window.location.pathname === '/login';
     }
    });
  }

  ngOnInit() {
  }

  getUsers() {
    this.router.navigate(['users']);
  }
  getArticles() {
    this.router.navigate(['articles']);
  }
  getCategories() {
    this.router.navigate(['categories']);
  }

  doLogout() {
    this.router.navigate(['login']);
  }

  goToLogin() {
    this.router.navigate(['login']);
  }

  goToNewArticle() {
    this.router.navigate(['create']);
  }
}
