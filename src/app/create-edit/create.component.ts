import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ArticleService} from '../services/article.service';
import {Category} from '../model/category';
import {CategoryService} from '../services/category.service';
import {Article} from '../model/article';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  categories: Category[] = [];
  body: string;
  title: string;
  category: Category;
  message: string;

  constructor(private router: Router, private articleService: ArticleService, private categoryService: CategoryService) {
  }

  ngOnInit() {
    if (localStorage.getItem('role')  !== 'USER') {
      this.router.navigate(['error']);
    } else {
      this.categoryService.getCategories().subscribe(data => {
        this.categories = data;
      });
    }
  }

  createArticle() {
    const art: Article = new Article;
    if (this.category !== null && this.category !== undefined) {art.categoryId = this.category.id;}
    art.title = this.title;
    art.author = localStorage.getItem('username');
    art.body = this.body;
    if (this.body === null || this.body === undefined || this.title === null || this.title === undefined) {
      this.message = 'You need to fill in the title and the body.';
    } else {
      console.log(art);
      this.articleService.createArticle(art).subscribe((resp) => {
        this.router.navigate(['articles']);
      }, (error1 => this.message = error1.error));
    }
  }

  onChangee(ev): void {
    this.category = ev.value;
  }
}
