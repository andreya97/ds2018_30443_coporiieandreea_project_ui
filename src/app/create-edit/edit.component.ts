import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ArticleService} from '../services/article.service';
import {Article} from '../model/article';
import {CategoryService} from '../services/category.service';
import {Category} from '../model/category';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./create.component.css']
})
export class EditComponent implements OnInit {

  id: number;
  article: Article;
  categories: Category[] = [];
  message: string;
  constructor(private router: Router, private route: ActivatedRoute, private articleService: ArticleService, private categoryService: CategoryService) {
  }

  ngOnInit() {
    if (localStorage.getItem('role')  !== 'USER') {
      this.router.navigate(['error']);
    } else {
      this.categoryService.getCategories().subscribe(data => {
        this.categories = data;
      });
      this.route.params.subscribe(params => {
        this.id = params['id'];
        this.articleService.getArticleById(this.id)
          .subscribe(data => {
            this.article = data;
            if (data.categoryId !== null) {
              this.categoryService.getCategoryById(data.categoryId).subscribe((a) => {
                this.article.category = a;
              });
            }
          });
      });
    }
  }
  onChange(ev): void {
    this.article.category = ev.value;
    this.article.categoryId =  this.article.category.id; // i am not very smart
  }

  updateArticle() {
    this.articleService.updateArticle(this.article).subscribe((res) => {
      this.router.navigate(['articles']);
    }, (error1 => this.message = error1.error));
 }
}
