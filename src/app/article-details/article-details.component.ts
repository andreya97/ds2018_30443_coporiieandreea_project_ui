import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Article} from '../model/article';
import {ArticleService} from '../services/article.service';
import {CategoryService} from '../services/category.service';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-art-details',
  templateUrl: './article-details.component.html',
  styleUrls: ['./article-details.component.css']
})
export class ArticleDetailsComponent implements OnInit {

  article: Article;
  id: number;
  disabled = false;
  uid = parseInt(localStorage.getItem('userid'), 10);
  loggedUser = localStorage.getItem('username');
  relatedArticles: Article[] = [];

  constructor(private route: ActivatedRoute, private articleService: ArticleService, private categoryService: CategoryService,
              private userService: UserService, private router: Router) {
  }

  ngOnInit() {
    this.relatedArticles = [];
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.articleService.getArticleById(this.id)
        .subscribe(data => {
          this.article = data;
          if (data.categoryId !== null) {
            this.categoryService.getCategoryById(data.categoryId).subscribe((a) => {
              // update category
              this.article.category = a;

              this.articleService.getRelatedArticles(this.article.id).subscribe((res) =>{
                this.relatedArticles = res;
              });

              // update disabled state
              if (a.subscriberIds && a.subscriberIds.indexOf(this.uid) !== -1) {
                this.disabled = true;
              }
            });
          }
        });
    });


  }

  subscribeUser(categoryid: number) {
    this.userService.subscribeUser(this.uid, categoryid).subscribe(response => {
      window.location.reload();
    });
  }

  unsubscribeUser(categoryid: number) {
    this.userService.unsubscribeUser(this.uid, categoryid).subscribe(response => {
      window.location.reload();
    });
  }

  editArticle(id: number) {
    this.router.navigate(['edit', id]);
  }

}
