import {Component, OnInit} from '@angular/core';
import {User} from '../model/user';
import {UserService} from '../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[] = [];
  displayedColumns: string[] = ['id', 'username', 'email', 'admin', 'delete'];
  message: string;
  user: User = new User();

  constructor(private router: Router, private userService: UserService) {
  }

  ngOnInit() {
    if (localStorage.getItem('role')  !== 'ADMIN') {
      this.router.navigate(['error']);
    } else {
      this.userService.getUsers()
        .subscribe(data => {
          this.users = data;
        }, error => this.message = error.error);
    }
  }

  deleteUser(id: number): void {
      this.userService.deleteUser(id).subscribe(iid => {
        window.location.reload();
        this.message = 'User ' + iid + ' has been successfully deleted;';
      }, error => this.message = error.error);
  }

  updateUser() {
    if (this.user.id === null || this.user.id === undefined) {
      this.message = 'Choose an id to update the user.';
    } else {
      this.userService.updateUser(this.user.id, this.user).subscribe(user => {
        window.location.reload();
        this.message = 'User ' + user.id + ' has been successfully updated;';
      }, error => this.message = error.error);
    }
  }

}
