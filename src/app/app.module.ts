import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {
  MatIconModule,
  MatMenuModule,
  MatToolbarModule,
  MatButtonModule,
  MatTableModule,
  MatFormFieldModule, MatInputModule, MatSelectModule
} from '@angular/material';
import {RouterModule} from '@angular/router';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {UsersComponent} from './users/users.component';
import {HomeComponent} from './home/home.component';
import {ArticleDetailsComponent} from './article-details/article-details.component';
import {HttpClientModule} from '@angular/common/http';
import {HeaderComponent} from './header/header.component';
import {LoginComponent} from './login/login.component';
import {ArticlesComponent} from './articles/articles.component';
import {FormsModule} from '@angular/forms';
import {CategoriesComponent} from './categories/categories.component';
import {CreateComponent} from './create-edit/create.component';
import {EditComponent} from './create-edit/edit.component';
import {ErrorComponent} from './error/error';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    HomeComponent,
    ArticleDetailsComponent,
    HeaderComponent,
    LoginComponent,
    ArticlesComponent,
    CategoriesComponent,
    CreateComponent,
    EditComponent,
    ErrorComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatMenuModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    HttpClientModule,
    MatSelectModule,
    RouterModule.forRoot([
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'users',
        component: UsersComponent
      },
      {
        path: 'categories',
        component: CategoriesComponent
      },
      {
        path: 'articles',
        component: ArticlesComponent
      },
      {
        path: 'article/:id',
        component: ArticleDetailsComponent
      },
      {
        path: 'create',
        component: CreateComponent
      },
      {
        path: 'edit/:id',
        component: EditComponent
      },
      {
        path: '',
        component: LoginComponent
      },
      {
        path: 'error',
        component: ErrorComponent
      },
      {
        path: 'home',
        component: HomeComponent
      }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
