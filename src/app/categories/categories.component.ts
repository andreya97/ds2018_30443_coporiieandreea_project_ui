import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Category} from '../model/category';
import {CategoryService} from '../services/category.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  categories: Category[] = [];
  displayedColumns: string[] = ['id', 'name', 'description', 'delete'];
  message: string;
  category: Category = new Category();

  constructor(private router: Router, private categoryService: CategoryService) {
  }

  ngOnInit() {
    if (localStorage.getItem('role')  !== 'ADMIN') {
      this.router.navigate(['error']);
    } else {
      this.categoryService.getCategories()
        .subscribe(data => {
          this.categories = data;
        }, error => this.message = error.error);
    }
  }

  createCategory() {
    this.categoryService.createCategory(this.category).subscribe(cat => {
      window.location.reload();
      this.message = 'Category has been successfully created;';
    }, error => this.message = error.error);
  }

  deleteCategory(id: number): void {
    this.categoryService.deleteCategory(id).subscribe(iid => {
      window.location.reload();
      this.message = 'Category ' + iid + ' has been successfully deleted;';
    }, error =>{console.log(JSON.stringify(error)); this.message = error.toString();});
  }

  updateCategory() {
    if (this.category.id === null || this.category.id === undefined) {
      this.message = 'Choose an id to update the category.';
    } else {
      this.categoryService.updateCategory(this.category.id, this.category).subscribe(cat => {
        window.location.reload();
        this.message = 'Category ' + cat.id + ' has been successfully updated;';
      }, error => this.message = error.toString());
    }
  }
}
