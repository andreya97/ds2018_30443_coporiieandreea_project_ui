import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Category} from '../model/category';
import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  categoryUrl = 'http://localhost:8080/newsagency/category';

  constructor(private http: HttpClient) {
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  getCategoryById(id: number) {
    return this.http.get<Category>(this.categoryUrl + '/get/' + id);
  }

  getCategories() {
    return this.http.get<Category[]>(this.categoryUrl + '/get/all');
  }

  createCategory(cat: Category) {
    return this.http.post<Category>(this.categoryUrl + '/create', cat, this.httpOptions);
  }

  deleteCategory(id: number) {
    return this.http.post<number>(this.categoryUrl + '/delete/' + id, this.httpOptions);
  }

  updateCategory(id: number, cat: Category) {
    return this.http.post<Category>(this.categoryUrl + '/update/' + id, cat, this.httpOptions);
  }
}
