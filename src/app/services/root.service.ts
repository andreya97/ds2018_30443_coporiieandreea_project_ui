import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../model/user';
import {LoginInfo} from '../model/logininfo';

@Injectable({
  providedIn: 'root'
})
export class RootService {

  rootURL = 'http://localhost:8080/newsagency/';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }

  register(user: User) {
    return this.http.post<User>(this.rootURL + 'register', user, this.httpOptions);
  }

  login(li: LoginInfo) {
    return this.http.post<User>(this.rootURL + 'login', li, this.httpOptions);
  }

}
