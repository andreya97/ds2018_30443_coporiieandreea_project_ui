import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  usersURL = 'http://localhost:8080/newsagency/user';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
 };


  constructor(private http: HttpClient) {
  }

  getUsers() {
    return this.http.get<User[]>(this.usersURL + '/get/all');
  }

  deleteUser(id: number) {
    return this.http.post<number>(this.usersURL + '/delete/' + id, this.httpOptions);
  }

  updateUser(id: number, user: User) {
    return this.http.post<User>(this.usersURL + '/update/' + id, user, this.httpOptions);
  }

  subscribeUser(id: number, catid: number) {
    // const hopts = {
    //   headers: new HttpHeaders({
    //     'Content-Type':  'application/json',
    //   }),
    //   params: new HttpParams().set('userid', id.toString()).set('categoryid', catid.toString()),
    // };
    console.log(this.usersURL + '/subscribe/?userid=' + id.toString() + '&categoryid=' + catid.toString());
    return this.http.post<void>(this.usersURL + '/subscribe/?userid=' + id.toString() + '&categoryid=' + catid.toString(), this.httpOptions);
  }

  unsubscribeUser(id: number, catid: number) {
    console.log(this.usersURL + '/subscribe/?userid=' + id.toString() + '&categoryid=' + catid.toString());
    return this.http.post<void>(this.usersURL + '/unsubscribe/?userid=' + id.toString() + '&categoryid=' + catid.toString(), this.httpOptions);
  }

}
