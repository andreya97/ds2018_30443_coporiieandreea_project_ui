import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Article} from '../model/article';
import {Category} from '../model/category';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  articlesUrl = 'http://localhost:8080/newsagency/article';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private http: HttpClient) {
  }

  getArticles() {
    return this.http.get<Article[]>(this.articlesUrl + '/get/all');
  }

  deleteArticle(id: number) {
    return this.http.post<number>(this.articlesUrl + '/delete/' + id, this.httpOptions);
  }

  getArticleById(id: number) {
    return this.http.get<Article>(this.articlesUrl + '/get/' + id);
  }

  getRelatedArticles(id: number) {
    return this.http.get<Article[]>(this.articlesUrl + '/get/related/' + id);
  }

  createArticle(article: Article) {
    return this.http.post<Article>(this.articlesUrl + '/create', article, this.httpOptions);
  }

  updateArticle(article: Article) {
    return this.http.post<Category>(this.articlesUrl + '/update/' + article.id, article, this.httpOptions);
  }
}
